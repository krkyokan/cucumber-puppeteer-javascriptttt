Feature: Adding product to cart

  Scenario: Adding the product to the cart
    Given I'm on my product page
    When I need to click the add to cart button
    Then I need to click the cart button and check the product
