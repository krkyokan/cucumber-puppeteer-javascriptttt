const puppeteer = require('puppeteer');
const assert = require("assert");
const {Given, When, Then} = require("@cucumber/cucumber");

var {setDefaultTimeout} = require('@cucumber/cucumber');
const timers = require("timers"); setDefaultTimeout(10000);

var browser;
var page;


Given('I\'m on my product page', async function () {
        const _browser = await puppeteer.launch({headless: false});
        browser = _browser;
        const _page = await browser.newPage();
        page = _page;
        await page.setViewport({ width: 1280, height: 1800 })
        await page.goto("https://www.modanisa.com/jersey-penye-sal--siyah--rabia-z.html");
        //await _page.waitForNavigation()
    });


    When('I need to click the add to cart button', async function () {
        await page.$eval('.basket-button', (b => b.click()));
    });

    Then('I need to click the cart button and check the product', async function () {

        await page.waitForSelector('.newHeaderFooter-basket-active');
        await page.goto("https://www.modanisa.com/basket/");
        let result = await page.$('.basketList-item') ? true : false;
        assert.strictEqual(result, true);
        await browser.close();
    });





